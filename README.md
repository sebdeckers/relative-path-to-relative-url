# relative-path-to-relative-url

Convert relative file system paths to relative URLs.

## Examples

```js
// Unix
relativePathToRelativeUrl('foo/bar')
relativePathToRelativeUrl('/foo/bar')
relativePathToRelativeUrl('./foo/bar')
// -> './foo/bar'

// Windows
relativePathToRelativeUrl('foo\\bar')
relativePathToRelativeUrl('\\foo\\bar')
relativePathToRelativeUrl('.\\foo\\bar')
// -> './foo/bar'
```

## relativePathToRelativeUrl(path)

`path` is a string containing a relative file path.

Returns a string representing a relative URL.

The URL always starts with `./` to make it relative to the current directory.

Respects the path separator of the host environment, typically `/` or `\`.

For example `./foo.js` or `foo.js` or `/foo` will all be returned as `./foo.js` on macOS and Linux, while backslashes will not be converted. Conversely, on Windows `.\foo.js` will be returned as `./foo.js`, while forward slashes will be left unchanged.

## See Also

- [relative-url-to-relative-path](https://www.npmjs.com/package/relative-url-to-relative-path)
- [slash](https://www.npmjs.com/package/slash)
