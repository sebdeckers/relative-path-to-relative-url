'use strict'

const path = require('path')
const assert = require('assert')

// Before - Unix
const originalSeparator = path.sep
path.sep = '/'
const relativePathToRelativeUrl = require('..')

assert.equal(relativePathToRelativeUrl('foo'), './foo', 'Bare (Unix)')
assert.equal(relativePathToRelativeUrl('./foo'), './foo', 'Current (Unix)')
assert.equal(relativePathToRelativeUrl('/foo'), './foo', 'Absolute (Unix)')
assert.equal(relativePathToRelativeUrl('foo/bar'), './foo/bar', 'Bare nested (Unix)')
assert.equal(relativePathToRelativeUrl('./foo/bar'), './foo/bar', 'Current nested (Unix)')
assert.equal(relativePathToRelativeUrl('/foo/bar'), './foo/bar', 'Absolute nested (Unix)')

// After
path.sep = originalSeparator
