'use strict'

const path = require('path')
const assert = require('assert')

// Before - Windows
const originalSeparator = path.sep
path.sep = '\\'
const relativePathToRelativeUrl = require('..')

assert.equal(relativePathToRelativeUrl('foo'), './foo', 'Bare (Windows)')
assert.equal(relativePathToRelativeUrl('.\\foo'), './foo', 'Current (Windows)')
assert.equal(relativePathToRelativeUrl('\\foo'), './foo', 'Absolute (Windows)')
assert.equal(relativePathToRelativeUrl('foo\\bar'), './foo/bar', 'Bare nested (Windows)')
assert.equal(relativePathToRelativeUrl('.\\foo\\bar'), './foo/bar', 'Current nested (Windows)')
assert.equal(relativePathToRelativeUrl('\\foo\\bar'), './foo/bar', 'Absolute nested (Windows)')

// After
path.sep = originalSeparator
