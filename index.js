'use strict'

const path = require('path')

const currentPath = '.'
const urlSlash = '/'

const separator = path.sep
const escapedSeparatorPattern = separator === '\\'
  ? '\\\\'
  : separator
const pathSeparatorRegex = new RegExp(escapedSeparatorPattern, 'g')

function relativePathToRelativeUrl (file) {
  const url = file.startsWith(currentPath) ? file
    : file.startsWith(path.sep) ? currentPath + file
    : currentPath + path.sep + file

  return path.sep === urlSlash
    ? url
    : url.replace(pathSeparatorRegex, urlSlash)
}

module.exports = relativePathToRelativeUrl
